import { IUser } from '../../../domain/dtos/user.dto';
import { ICreateUser } from '../../../domain/dtos/user.dto';
import User from "../../entities/user.entity";

export default interface IUserRepository {
    findMany(params?: IUser): Promise<User[]>
    findOne(id: string): Promise<User | undefined>
    create(payload: ICreateUser): Promise<User>
    save(user: User): Promise<User>
    delete(id: string): Promise<void>
}