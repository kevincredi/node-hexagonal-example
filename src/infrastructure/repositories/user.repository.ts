import { ICreateUser, IUser } from './../../domain/dtos/user.dto';
import { getRepository, Repository } from "typeorm";
import User from "../entities/user.entity";
import IUserRepository from './interfaces/user.repository.interface';

class UserRepository implements IUserRepository {
    constructor (
        private repository: Repository<User> = getRepository(User)
    ) {}

    public async findMany(params?: IUser): Promise<User[]> {        
        return this.repository.find({where: {...params}})
    }
    
    public async findOne(id: string): Promise<User | undefined> {
        return this.repository.findOne(id)
    }

    public async create(payload: ICreateUser): Promise<User> {
        const user = this.repository.create(payload)

        return this.repository.save(user)
    }

    public async save(user: User): Promise<User> {
        return this.repository.save(user)
    }

    public async delete(id: string): Promise<void> {
        this.repository.delete(id)
    }
}

export default UserRepository;