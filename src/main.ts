import App from "./app";

new App()
  .setupDatabase()
  .setupHttpServer()