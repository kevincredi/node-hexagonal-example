import UserService from '../../../domain/services/user.service';
import { IRequest, IResponse } from '..';
import { IController } from './interfaces/controller.interface';
import { userSchema } from './schemas/user.schema';
import { schema } from './schemas';


class UserController implements IController {
    constructor(
        private service: UserService = new UserService()
    ) {}

    async get(request: IRequest, response: IResponse) {        
        return response.json(await this.service.get(request.query))                
    }

    async show(id: string, request: IRequest, response: IResponse) {
        return response.json(await this.service.show(id))        
    }

    async create(request: IRequest, response: IResponse) {
        schema.validator(userSchema, request.body)

        return response.status(201).json(await this.service.create(request.body))
    }

    async update(id: string, request: IRequest, response: IResponse) {
        schema.validator(userSchema, request.body)
        
        return response.json(await this.service.update(id, request.body))
    }

    async destroy(id: string, request: IRequest, response: IResponse) {
        // 
    }
}

export default UserController