import Joi from "joi";

const userSchema = Joi.object().keys({
  name: Joi.string().required(),
  email: Joi.string().email().required()
})

export { userSchema }