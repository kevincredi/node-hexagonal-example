import { AnySchema } from "joi";
import ExceptionHandler from "../../../../domain/exceptions";

const validator = (schema: AnySchema, payload: any) => {
  const { error } = schema.validate(payload)
  if (error) {
    throw new ExceptionHandler(error.details[0].message, 422)
  }

  return true
}

// Add functions for this schema
const schema = {
  validator,
}

export { schema }