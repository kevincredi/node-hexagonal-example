import { IRequest, IResponse } from "../.."

interface IController {
  get(request: IRequest, response: IResponse): void
  show(id: string, request: IRequest, response: IResponse): void
  create(request: IRequest, response: IResponse): void
  update(id: string, request: IRequest, response: IResponse): void
  destroy(id: string, request: IRequest, response: IResponse): void
}

export { IController }