import exceptionMiddleware from '../middlewares/exception.middleware';
import { http } from '../resources';
import userRoutes from './user.route';

const router = http.router()

/** 
 * Add general middlewares
 */
http.handleMiddlewares({
  router,
  middlewares: []
})

/**
 * Routes
 */
router.use('/users', userRoutes)

/**
 * Exception Middleware
 */
 router.use(exceptionMiddleware)

export default router