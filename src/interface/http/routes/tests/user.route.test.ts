import { createConnection, getConnection } from 'typeorm';
import request from 'supertest'
import router from '..'
import { http } from '../../resources'
import { databaseConfig } from '../../../../app/config/database';

const server = http.server()
  server.use(http.server.json())
  server.use(router)

describe(('user.route'), () => {
  beforeAll(async () => {    
    await createConnection(databaseConfig)
  })  
  
  afterAll(() => {
    getConnection().close()
  })

  it('get users', async () => {    
    const response = await request(server).get('/users')
    expect(response.status).toBe(200)
  })

  it('get user by id', async () => {    
    const response = await request(server).get('/users/45fbb40b-8de3-4181-931e-4bb3266a6114')    
    expect(response.status).toBe(200)
  })

  it('get not found user id', async () => {    
    const response = await request(server).get('/users/x')
    expect(response.status).toBe(404)
  })
})