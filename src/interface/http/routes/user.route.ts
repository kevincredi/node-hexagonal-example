import { http } from '../resources';
import UserController from '../controllers/user.controller';
import userMiddleware from '../middlewares/user.middleware';

const userRoutes = http.router()

/**
 * Implements CRUD operations for user routes
*/
http.resource({
  router: userRoutes,
  controller: UserController,
  middlewares: [userMiddleware]
})

export default userRoutes