import { INextFunction, IRequest, IResponse, IMiddleware } from "..";

/**
 * Middleware validations
 */
const userMiddleware: IMiddleware = async (request: IRequest, response: IResponse, next: INextFunction) => {    
    next()
}

export default userMiddleware