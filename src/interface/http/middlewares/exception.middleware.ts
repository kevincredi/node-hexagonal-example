import { IError, INextFunction, IRequest, IResponse } from "..";

const exceptionMiddleware = (error: IError, request: IRequest, response: IResponse, next: INextFunction) => {
  if (error) {
    return response.status(error.statusCode || 500).json({
      message: error.message,
    })
  }

  next()
}

export default exceptionMiddleware