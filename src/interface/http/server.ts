import * as dotenv from 'dotenv';
import { httpConfig } from '../../app/config/http';
import { http } from './resources';
import router from './routes';

dotenv.config()

const server = http.server()

const httpServer = () => {    
    server.use(http.server.json())
    server.use(router)    
    server.listen(httpConfig.port, () => {
        console.log(`SERVER LISTENING PORT ${httpConfig.port}`)
    })
}

export { httpServer }