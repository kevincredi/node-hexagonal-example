import UserRepository from "../../infrastructure/repositories/user.repository";
import { IUser, ICreateUser } from "../dtos/user.dto";
import ExceptionHandler from "../exceptions";
import { IUserService } from "./interfaces/user.service.interface";

class UserService implements IUserService {
    userRepository: UserRepository

    constructor() {
        this.userRepository = new UserRepository()
    }

    async get(params?: IUser): Promise<IUser[]> {        
        return this.userRepository.findMany(params)
    }

    async show(id: string): Promise<IUser | undefined> {
        const user = await this.userRepository.findOne(id)
        if (!user) {
            throw new ExceptionHandler('User not found', 404)
        }

        return user
    }

    async create(payload: ICreateUser): Promise<IUser> {         
        return this.userRepository.create(payload)
    }

    async update(id: string, payload: IUser): Promise<IUser> {
        const user = await this.userRepository.findOne(id)
        if (!user) {
            throw new ExceptionHandler('User not found', 404)
        }

        user.email = payload.email ?? user.email
        user.name = payload.name ?? user.name

        return this.userRepository.save(user)
    }

    async destroy(id: string): Promise<void> {
        // 
    }
}

export default UserService