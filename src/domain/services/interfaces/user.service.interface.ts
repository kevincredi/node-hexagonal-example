import { ICreateUser, IUser, IUserId } from '../../dtos/user.dto';

interface IUserService {
  /**
   * Implement reference repository
   */
  userRepository: any

  get(params?: IUser): Promise<IUser[]>
  show(id: IUserId): Promise<IUser|undefined>
  create(payload: ICreateUser): Promise<IUser>
  update(id: IUserId, payload: IUser): Promise<IUser>
  destroy(id: IUserId): Promise<void>
}

export { IUserService }