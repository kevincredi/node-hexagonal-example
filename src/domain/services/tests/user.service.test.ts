import { createConnection, getConnection } from 'typeorm';
import { databaseConfig } from '../../../app/config/database';
import User from '../../../infrastructure/entities/user.entity';
import ExceptionHandler from '../../exceptions';
import UserService from "../user.service"

describe('UserService', () => {
  beforeAll(async () => {    
    await createConnection(databaseConfig)
  })  
  
  afterAll(() => {
    getConnection().close()
  })

  test('get user by id', async () => {
    const userService = new UserService()
    const user = await userService.show('45fbb40b-8de3-4181-931e-4bb3266a6114')

    expect(user).toBeInstanceOf(User)
  })

  test('should throw exception user not found', async () => {
    const userService = new UserService()
    
    try {
      const user = await userService.show('x')    
    } catch (error) {
      console.log(error)
      expect((error as ExceptionHandler).statusCode).toEqual(404)
    }
  })
})