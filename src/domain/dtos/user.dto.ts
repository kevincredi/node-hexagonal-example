type IUserId = string

interface IUser {
    id?: IUserId
    name?: string
    email?: string
    created_at?: Date
    updated_at?: Date
}

type ICreateUser = Pick<IUser, 'name' | 'email'> 

export { IUser, ICreateUser, IUserId }