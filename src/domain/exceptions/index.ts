class ExceptionHandler extends Error {
  constructor(
      message: string,      
      public statusCode: number = 500
    ) {
    super(message)
    this.name = 'EXCEPTION_HANDLER'
  }
}

export default ExceptionHandler