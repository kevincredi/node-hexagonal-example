import { createConnection } from 'typeorm';
import { modules } from './config/config';
import { httpServer } from '../interface/http/server';
import { databaseConfig } from './config/database';

class App {
    setupHttpServer() {
        if (modules?.http) {
            httpServer()
        }

        return this
    }

    setupDatabase() {
        if (modules?.database) {
            createConnection(databaseConfig)
                .then(() => {
                    console.log('DATABASE CONNECTED')
                })
                .catch(error => {
                    console.log('DATABASE CONNECTION FAIL', error)
                })
        }

        return this
    }
}

export default App