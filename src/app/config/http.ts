import * as dotenv from 'dotenv';
import { IHttpConfig } from '../../interface/http';

dotenv.config()

const httpConfig: IHttpConfig = {
  port: Number(process.env.APP_PORT || 3000)  
}

export { httpConfig }