import { ConnectionOptions, createConnection } from "typeorm";
import * as dotenv from 'dotenv';

dotenv.config()

const databaseConfig: ConnectionOptions = {
    name: 'default',
    type: 'mysql',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASS,
    entities: ['src/infrastructure/entities/*.ts'],
    synchronize: false   
}

export { databaseConfig }