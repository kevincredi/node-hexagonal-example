/**
 * Enable application modules
 */
const modules = {
    http: true,
    database: true,
}

export { modules }